﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter a string variable: ";
    std::string name;
    std::getline(std::cin, name);

    std::cout << "Your varitable: " << name << std::endl;
    std::cout << "It's length: " << name.length() << std::endl;
    std::cout << "It's first char: " << name[0] << std::endl;
    std::cout << "It's last char: " << name[name.length() - 1] << std::endl;

    return 0;
}
